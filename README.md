## Introduction

Datazoom is a high availability real-time data collection solution. This tvOS project builds the framework file that will be distributed. This document summarizes how to integrate the DataZoom framework with applications.
    

Version 1.0  : Framework [Tracked with the help of Player's instance, in all required Controllers/Views].


## tvOS - Native : Framework Integration.


### Initial setup:

* The DZNativeConnector.framework file should be downloaded from **[Here](https://gitlab.com/datazoom/apple-tvos/tvos-collector-libraries-internal/blob/master/Native%20Collector/1.8/DZNativeCollector.framework.zip)** .

* Drag and drop the downloaded framework file into Xcode. Drag the file to the files inspector on the left side of Xcode. Make sure you check the check box "Copy items" in the popup menu comes while drag and drop the file.


### Steps for Swift Language based Applications:

* After including the framework file, open the ViewController/View file, where the AVPlayer(native player) is included.

* Import the framework using the following command,

               import DZNativeCollector


* Initialise the framework by passing along the 'Configuration ID',

               DZNativeCollector.dzConnectorManager.initNativePlayerWith(configID: <provide config is string>, url: <provide connection url dev/staging>, playerInstance: videoPlayer)
  

* Run the app and observe the events in Amplitude, data corresponding to MOBILE/iPhone in Platform, refers to the events tracked from iPhone.


### Steps for ObjectiveC Language based Applications:

* After including the framework file, Create a bridging header file, to allow interoperability of languages.
    
* open the ViewController/View file, where the AVPlayer(native player) is included.
    
* Import the following,

           #import <AVKit/AVKit.h>

           #import <AVFoundation/AVFoundation.h>

           #import <DZNativeCollector/DZNativeCollector.h>

* Initialise the swift class in the .h file.

                 DZNativeCollector *dzObject;

* In the .m file, allocate using,

                 dzObject = [[DZNativeCollector alloc]init];

                [dzObject initialiseWithConfigId: <provide config is string>, url: <provide connection url dev/staging>, playerInstance: videoPlayer];


* Run the app and observe the events in Amplitude, data corresponding to MOBILE/iPhone in Platform, refers to the events tracked from iPhone.

## Demo Application

* A demo application that shows the usage of this framework is available **[Here](https://gitlab.com/datazoom/tvos-demos/tvos-native-demo/tree/develop)** .

## Credit
  - Vishnu M P

## Link to License/Confidentiality Agreement
Datazoom, Inc ("COMPANY") CONFIDENTIAL
Copyright (c) 2017-2018 [Datazoom, Inc.], All Rights Reserved.
NOTICE:  All information contained herein is, and remains the property of COMPANY. The intellectual and technical concepts contained
herein are proprietary to COMPANY and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
from COMPANY.  Access to the source code contained herein is hereby forbidden to anyone except current COMPANY employees, managers or contractors who have executed
Confidentiality and Non-disclosure agreements explicitly covering such access.
The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
